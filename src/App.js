import {Fragment, useState, useEffect} from 'react';

import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";

import {Container} from "react-bootstrap"

import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css'; 

import {UserProvider} from './UserContext';

/*components*/
import AppNavbar from './components/AppNavbar';

/*pages*/
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';

import ErrorPage from './components/ErrorPage';

export default function App() {

  /*React Context is nothing but a global state to the app. It is a way to make a particular data available to all the components no matter how they are nested. context helps you broadcast the data and changes happening to that data, to all components*/

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  //function for clearing local storage on logout
  const unsetUser = () => {
    localStorage.clear();

    setUser({
                id: null,
                isAdmin: null,
            })
  }

  useEffect ( () => {

    fetch("http://localhost:4000/api/users/details", {
      headers: {
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then (response => response.json())
    .then (data => {
      console.log (data) //object - user details
      if(typeof data._id != "undefined")
      {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
    })
  }, [])

  console.log(user)

  return(
    <UserProvider value={{setUser, unsetUser, user}}>
      <BrowserRouter>
        <AppNavbar />
          <Container fluid className="m-3">
        <Switch>
            <Route exact path ="/" component = {Home}/>
            <Route exact path ="/courses" component = {Courses}/>
            <Route exact path ="/register" component = {Register}/>
            <Route exact path ="/login" component = {Login}/>
            <Route exact path ="/logout" component = {Logout}/>
            <Route exact path ="*" component = {ErrorPage}/>
        </Switch>
          </Container>
      </BrowserRouter>
   </UserProvider>
  )
}

